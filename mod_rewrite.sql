-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Хост: em386214.mysql.ukraine.com.ua
-- Время создания: Авг 07 2020 г., 08:24
-- Версия сервера: 5.7.16-10-log
-- Версия PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `em386214_woo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `mod_rewrite`
--

CREATE TABLE `mod_rewrite` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mod_rewrite`
--

INSERT INTO `mod_rewrite` (`id`, `text`, `views`) VALUES
(123, 'Привет Женя', 29),
(345, 'Привет Саша', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `mod_rewrite`
--
ALTER TABLE `mod_rewrite`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
